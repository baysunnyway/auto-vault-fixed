// import axios from "axios";
import axios, { AxiosError } from "@/plugins/axiosInstance";

export default function setup() {
    axios.interceptors.response.use(function (response) {
        return response
    }, function (error) {

        if (error.response.status === 401 || error.response.status === 419) {
            localStorage.clear();
        }

        return Promise.reject(error);
    });
}