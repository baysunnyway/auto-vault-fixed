import numeral from "numeral"

export default function numberFormat(value: any) {
    return numeral(value).format('0,0')
}

export function formatNumberToBillion(number) {
  if (typeof number !== 'number') {
    return ''; // Return an empty string for non-numeric values
  }

  const billion = (number / 1000000000).toFixed(9).replace(/\.?0+$/, ''); // Fixed to 9 decimal places and remove trailing zeros
  return `IDR/RP ${billion} billion`;
}

export function dateFormat(sDate){
    const bookingStartAt = new Date(sDate);
    if (isNaN(bookingStartAt)) {
        return ''; // Return an empty string for invalid dates
    }
    const formattedDate = bookingStartAt.toLocaleDateString('en-GB', {
      day: '2-digit',
      month: '2-digit',
      year: 'numeric'
    });
    const formattedTime = bookingStartAt.toLocaleTimeString('en-GB', {
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit',
      hour12: false
    });
    return `${formattedDate.replace(/\//g, '-')} ${formattedTime}`;
}

export function htmlFormat(messedUp){
  const element = document.createElement('div');
  element.innerHTML = messedUp;
  return element.innerHTML;
}