import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://autovault.forestadev.tech', // Replace with your desired base URL
  // Other default configuration options
});

export default instance;