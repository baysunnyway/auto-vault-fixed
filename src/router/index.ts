// Composables
import DefaultVue from '@/views/layouts/default/Default.vue'
import { useAuthStore } from '@/store/auth'
import _ from 'lodash'
import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    component: () => import('@/views/layouts/default/Default.vue'),
    children: [
      {
        path: '',
        redirect: { name: 'dashboard' }
      },
      {
        path: '/dashboard',
        name: 'dashboard',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "home" */ '@/views/Dashboard.vue'),
      },
      {
        path: '/floor-plan',
        name: 'floor_plan',
        component: () => import('@/views/FloorPlan.vue')
      }
    ],
  },
  {
    path: '/customer',
    component: () => import('@/views/layouts/default/Default.vue'),
    children: [
      {
        path: '',
        name: 'customer',
        component: () => import('@/views/menu/customer/Index.vue')
      },
      {
        path: 'detail_customer/:id?',
        name: 'detail_customer',
        component: () => import('@/views/menu/customer/Detail.vue')
      },
      {
        path: 'add',
        name: 'add_customer',
        component: () => import('@/views/menu/customer/Add.vue')
      },
      {
        path: 'edit/:id?',
        name: 'edit_customer',
        component: () => import('@/views/menu/customer/Add.vue')
      },
      {
        path: 'add_point/:id?',
        name: 'add_point',
        component: () => import('@/views/menu/customer/Point.vue')
      },
      {
        path: 'update_membership/:id?',
        name: 'update_membership',
        component: () => import('@/views/menu/customer/Membership.vue')
      },
    ]
  },
  {
    path: '/staff',
    component: () => import('@/views/layouts/default/Default.vue'),
    children: [
      {
        path: '',
        name: 'staff',
        component: () => import('@/views/menu/staff/Index.vue')
      },
      {
        path: 'detail_staff/:id?',
        name: 'detail_staff',
        component: () => import('@/views/menu/staff/Detail.vue')
      },
      {
        path: 'add',
        name: 'add_staff',
        component: () => import('@/views/menu/staff/Add.vue')
      },
      {
        path: 'edit/:id?',
        name: 'edit_staff',
        component: () => import('@/views/menu/staff/Add.vue')
      }
    ]
  },
  {
    path: '/vehicle',
    component: () => import('@/views/layouts/default/Default.vue'),
    children: [
      {
        path: '',
        name: 'vehicle',
        component: () => import('@/views/menu/vehicle/Index.vue')
      },
      {
        path: 'detail/:id?',
        name: 'detail_vehicle',
        component: () => import('@/views/menu/vehicle/Detail.vue')
      }
    ]
  },
  {
    path: '/promo',
    component: () => import('@/views/layouts/default/Default.vue'),
    children: [
      {
        path: '',
        name: 'promo',
        component: () => import('@/views/menu/promo/Index.vue')
      },
      {
        path: 'add',
        name: 'add_promo',
        component: () => import('@/views/menu/promo/Add.vue')
      },
      {
        path: 'edit/:id?',
        name: 'edit_promo',
        component: () => import('@/views/menu/promo/Add.vue')
      }
    ]
  },
  {
    path: '/addon',
    component: DefaultVue,
    children: [
      {
        path: '',
        name: 'addon',
        component: () => import('@/views/menu/addon/Index.vue')
      },
      {
        path: 'add',
        name: 'add_addon',
        component: () => import('@/views/menu/addon/Add.vue')
      },
      {
        path: 'edit/:id?',
        name: 'edit_addon',
        component: () => import('@/views/menu/addon/Add.vue')
      }
    ]
  },
  {
    path: '/item_storage',
    component: DefaultVue,
    children: [
      {
        path: '',
        name: 'item_storage',
        component: () => import('@/views/menu/item_storage/Index.vue')
      },
      {
        path: 'add',
        name: 'add_item_storage',
        component: () => import('@/views/menu/item_storage/Add.vue')
      },
      {
        path: 'edit/:id?',
        name: 'edit_item_storage',
        component: () => import('@/views/menu/item_storage/Add.vue')
      }
    ]
  },
  {
    path: '/insurance',
    component: DefaultVue,
    children: [
      {
        path: '',
        name: 'insurance',
        component: () => import('@/views/menu/insurance/Index.vue')
      },
      {
        path: 'add',
        name: 'add_insurance',
        component: () => import('@/views/menu/insurance/Add.vue')
      },
      {
        path: 'edit/:id?',
        name: 'edit_insurance',
        component: () => import('@/views/menu/insurance/Add.vue')
      }
    ]
  },
  {
    path: '/task',
    component: DefaultVue,
    children: [
      {
        path: '',
        name: 'task',
        component: () => import('@/views/menu/task/Index.vue')
      },
      {
        path: 'add',
        name: 'add_task',
        component: () => import('@/views/menu/task/Add.vue')
      },
      {
        path: 'detail/:id?',
        name: 'detail_task',
        component: () => import('@/views/menu/task/Detail.vue')
      },
      {
        path: 'edit/:id?',
        name: 'edit_task',
        component: () => import('@/views/menu/task/Add.vue')
      },
    ]
  },
  {
    path: '/transaction',
    component: DefaultVue,
    children: [
      {
        path: '',
        name: 'transaction',
        component: () => import('@/views/menu/transaction/Index.vue')
      },
      {
        path: 'payment/:id?',
        name: 'payment',
        component: () => import('@/views/menu/transaction/Add.vue')
      },
      {
        path: 'detail_transaction/:id?',
        name: 'detail_transaction',
        component: () => import('@/views/menu/transaction/Detail.vue')
      },
    ]
  },
  {
    path: '/booking',
    component: DefaultVue,
    children: [
      {
        path: '',
        name: 'booking',
        component: () => import('@/views/menu/booking/Index.vue')
      },
      {
        path: 'detail/:id?',
        name: 'detail_booking',
        component: () => import('@/views/menu/booking/Detail.vue')
      }
    ]
  },
  {
    path: '/setting',
    component: DefaultVue,
    children: [
      {
        path: '',
        name: 'setting',
        component: () => import('@/views/menu/setting/Index.vue')
      },
      {
        path: 'edit/:id?',
        name: 'edit_setting',
        component: () => import('@/views/menu/setting/Add.vue')
      }
    ]
  },
  {
    path: '/appointment',
    component: DefaultVue,
    children: [
      {
        path: '',
        name: 'appointment',
        component: () => import('@/views/menu/appointment/Index.vue')
      },
      {
        path: 'detail/:reference_number?',
        name: 'detail_appointment',
        component: () => import('@/views/menu/appointment/Detail.vue')
      }
    ]
  },
  {
    path: '/user',
    component: () => import('@/views/layouts/AuthLayout.vue'),
    children: [
      {
        path: 'login',
        name: 'Login',
        component: () => import('@/views/Login.vue')
      }
    ]
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

router.beforeEach(async (to, from) => {
  const authStore = useAuthStore();

  // Empty token state means unauthenticated
  if (_.isEmpty(authStore.accessToken) && to.name !== 'Login') {
    router.push({ name: 'Login' });
  }
})

export default router
