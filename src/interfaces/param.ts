export interface GetParamApi {
    page: number
    per_page: number
    filters: Array<FilterInfo> | undefined
    orders: Array<SortOrder>
    keyword: string
}

interface FilterInfo {
    field: string
    operator: string
    value: string
}

interface SortOrder {
    field: string
    direction: string
}
