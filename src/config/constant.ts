export const constant = {
    USER: {
        CUSTOMER_TYPE: [
            {
                title: 'Individual',
                value: 'individual'
            },
            {
                title: 'Business',
                value: 'business'
            }
        ],
        IDENTITY_TYPE: [
            {
                title: 'Passport',
                value: 'passport'
            },
            {
                title: 'Identification Card',
                value: 'identification_card'
            },
            {
                title: 'Company Identification Card',
                value: 'company_identification_card'
            }
        ]
    },
    PAYMENT: {
        TYPE: [
            {
                title: 'Manual Bank Transfer',
                value: 'manual_bank_transfer'
            },
            {
                title: 'Point',
                value: 'point'
            }
        ]
    },
    PROMO: {
        TYPE: {
            PERCENT: {
                name: 'Percent',
                id: 'percent'
            },
            FIXED: {
                name: 'Fixed',
                id: 'fixed'
            }
        }
    },
    ROLE: {
        STAFF: {
            id: 2,
            name: 'Staff'
        },
        CUSTOMER: {
            id: 3,
            name: 'Customer'
        }
    }
}