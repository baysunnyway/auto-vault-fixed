/**
 * main.ts
 *
 * Bootstraps Vuetify and other plugins then mounts the App`
 */

// Components
import App from './App.vue'
import './assets/main.css'

//Vue easy data table
import Vue3EasyDataTable from 'vue3-easy-data-table';
import 'vue3-easy-data-table/dist/style.css';

//Vue toast notification
import VueToast from 'vue-toast-notification'
import 'vue-toast-notification/dist/theme-sugar.css';

// Composables
import { createApp } from 'vue'

// Plugins
import { registerPlugins } from '@/plugins'
import axios from 'axios'
import interceptorSetup from '../interceptors'

const app = createApp(App)
const token = localStorage.getItem("access_token");

axios.defaults.baseURL = import.meta.env.VITE_APP_BASE_URL;
axios.defaults.headers.common["Accept"] = "application/json";

if (token) {
    axios.defaults.headers['Authorization'] = 'Bearer ' + token;
}

interceptorSetup();

registerPlugins(app)
app.component('EasyDataTable', Vue3EasyDataTable);

app.use(VueToast, {
    // One of the options
    position: 'top'
})

app.mount('#app')
