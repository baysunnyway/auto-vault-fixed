import { defineStore } from "pinia";
import { ref } from "vue";

export const useUserStore = defineStore('user', () => {

    const user = ref<UserInfo>(JSON.parse(localStorage.getItem('user') ?? '{}'));

    function set(responseUser: UserInfo) {
        user.value = responseUser;
        localStorage.setItem('user', JSON.stringify(responseUser));
    }

    return { user, set };
});
