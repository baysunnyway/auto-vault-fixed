export const serverOptionsDefault: serverOptions = {
	page: 1,
	rowsPerPage: 10,
	sortBy: 'id',
	sortType: 'desc'
}
