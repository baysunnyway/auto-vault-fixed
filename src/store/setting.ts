import { GetParamApi } from "@/interfaces/param"
import router from "@/router";
// import axios, { AxiosError } from "axios"
import axios, { AxiosError } from "../plugins/axiosInstance";
import _ from "lodash";
import { defineStore } from "pinia"
import { ref } from "vue"
import {useToast} from 'vue-toast-notification';

export const useSettingStore = defineStore('setting', () => {

    const $toast = useToast()

    const initialState: SettingInfo = {
        id: null,
        display_name: '',
        value: ''
    }

    const settings = ref<ResponseSettingApi>({
        current_page: 0,
        data: [],
        to: 0,
        total: 0
    })
    const setting = ref<SettingInfo>(initialState)

    async function getAll(paramsApi: GetParamApi = {
        page: 1,
        per_page: 5,
        filters: [],
        orders: [],
        keyword: ''
    }) {
        try {
            const response = await axios.get('/api/admin/settings/', {
                params: paramsApi
            });
            
            settings.value = response.data.result
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.result, { position: 'top' })
        }
    }

    async function update(id: string, param: SettingInfo){
        try {
            const response = await axios.patch('/api/admin/settings/' + id, {
                key: param.display_name,
                value: param.value
            });

            $toast.success(response.data.result, { position: 'top' })
            router.push({ name: 'setting' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

    function $reset(){
        setting.value = initialState
    }

    return { settings, setting, getAll, update, $reset }
})