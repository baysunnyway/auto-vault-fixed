import { GetParamApi } from "@/interfaces/param";
// import axios, { AxiosError } from "axios";
import axios, { AxiosError } from "../plugins/axiosInstance";
import { defineStore } from "pinia";
import { ref } from "vue";
import { useErrorStore } from "./error";
import { useToast } from 'vue-toast-notification';
import moment from "moment";
import router from "@/router";

export const usePromoStore = defineStore('promo', () => {

	const errorStore = useErrorStore();
	const $toast = useToast()

	const initialPromoState: PromoInfo = {
		id: null,
		is_active: true,
		title: '',
		description: '',
		promo_type: {
			id: 'percent',
			name: 'Percent'
		},
		amount: 0,
		code: '',
		is_combinable: false,
		quota: 0,
		started_at: moment().format('YYYY-MM-DD'),
		ended_at: moment().format('YYYY-MM-DD'),
		created_at: moment().format('YYYY-MM-DD'),
		usages_count: 0
	}

	const promos = ref<ResponseStaffApi>({
		current_page: 0,
		data: [],
		to: 0,
		total: 0
	})
	const promo = ref<PromoInfo>(initialPromoState)

	async function getAll(paramsApi: GetParamApi = {
		page: 1,
		per_page: 5,
		filters: [],
		orders: [],
		keyword: ''
	}) {
		try {
			const response = await axios.get('/api/admin/promos', {
				params: paramsApi
			});

			promos.value = response.data.result
		} catch (error: any | AxiosError) {
			errorStore.set(error);
		}
	}

	async function get(id: string) {
		try {
			const response = await axios.get('/api/admin/promos/' + id);

			promo.value = response.data.result
		} catch (error: any | AxiosError) {
			errorStore.set(error);
		}
	}

	async function insert(promoParam: PromoInfo){
        try {
            const response = await axios.post('/api/admin/promos/', {
				...promoParam,
				promo_type: promoParam.promo_type.id
			});

            $toast.success(response.data.result, { position: 'top' })
            router.push({ name: 'promo' })
        } catch (error: any | AxiosError) {
            const errorData = error.response.data
			const message = errorData.result.message ?? errorData.message
            $toast.error(message, { position: 'top' })
        }
    }

    async function update(promoParam: PromoInfo){
        try {
            const response = await axios.patch('/api/admin/promos/' + promoParam.id, {
				...promoParam,
				// promo_type: promoParam.promo_type.id
			});


            $toast.success(response.data.result, { position: 'top' })
            router.push({ name: 'promo' })
        } catch (error: any | AxiosError) {
			$toast.error(error.response.data.message, { position: 'top' })
        }
    }

	async function remove(id: string){
        try {
            const response = await axios.delete('/api/admin/promos/' + id);

            $toast.success(response.data.result, { position: 'top' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

	function $reset() {
		promo.value = initialPromoState
	}

	return { promos: promos, promo: promo, getAll, get, insert, update, remove, $reset };
})
