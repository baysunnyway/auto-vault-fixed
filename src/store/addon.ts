import { GetParamApi } from "@/interfaces/param"
import router from "@/router";
// import axios, { AxiosError } from "axios"
import axios, { AxiosError } from "../plugins/axiosInstance";
import _ from "lodash";
import { defineStore } from "pinia"
import { ref } from "vue"
import {useToast} from 'vue-toast-notification';

interface ResponseAddOnApi {
    current_page: number
    data: Array<AddOnInfo>
    to: number
    total: number
}

interface AddOnInfo {
    id: number | null
    title: string
    description: string
    amount: number
}

export const useAddOnStore = defineStore('addon', () => {

    const $toast = useToast()

    const initialStateAddOn = {
        id: null,
        title: '',
        description: '',
        amount: 0
    }

    const addons = ref<ResponseAddOnApi>({
        current_page: 0,
        data: [],
        to: 0,
        total: 0
    })
    const addon = ref<AddOnInfo>(initialStateAddOn)

    async function getAll(paramsApi: GetParamApi = {
        page: 1,
        per_page: 5,
        filters: [],
        orders: [],
        keyword: ''
    }) {
        try {
            const response = await axios.get('/api/admin/addon-services', {
                params: paramsApi
            });
            
            addons.value = response.data.result
        } catch (error: any | AxiosError) {
            
        }
    }

    async function get(id: string){
        try {
            const response = await axios.get('/api/admin/addon-services/' + id);
            
            addon.value = response.data.result
        } catch (error: any | AxiosError) {
  
        }
    }

    async function insert(addOnParam: AddOnInfo){
        try {
            const response = await axios.post('/api/admin/addon-services/', addOnParam);

            $toast.success(response.data.result, { position: 'top' })
            router.push({ name: 'addon' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

    async function update(addOnParam: AddOnInfo){
        try {
            const response = await axios.patch('/api/admin/addon-services/' + addOnParam.id, addOnParam);

            $toast.success(response.data.result, { position: 'top' })
            router.push({ name: 'addon' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

    async function remove(id: string){
        try {
            const response = await axios.delete('/api/admin/addon-services/' + id);

            $toast.success(response.data.result, { position: 'top' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

    function $reset(){
        addon.value = initialStateAddOn
    }

    return { addons, addon, getAll, get, insert, update, remove, $reset }
})