import { GetParamApi } from "@/interfaces/param"
import router from "@/router";
// import axios, { AxiosError } from "axios"
import axios, { AxiosError } from "../plugins/axiosInstance";
import _ from "lodash";
import { defineStore } from "pinia"
import { ref } from "vue"
import {useToast} from 'vue-toast-notification';

export const useItemStorageStore = defineStore('item_storage', () => {

    const $toast = useToast()

    const initialStateItemStorage = {
        id: null,
        code: '',
        name: '',
        length: 0,
        width: 0,
        height: 0,
        status: '',
        rent_service: {
            id: 0,
            rentable_type: 'storage',
            amount: 0
        }
    }

    const itemStorages = ref<ResponseItemStorageApi>({
        current_page: 0,
        data: [],
        to: 0,
        total: 0
    })
    const itemStorage = ref<ItemStorageInfo>(initialStateItemStorage)

    async function getAll(paramsApi: GetParamApi = {
        page: 1,
        per_page: 5,
        filters: [],
        orders: [],
        keyword: ''
    }) {
        try {
            const response = await axios.get('/api/admin/item-storages/', {
                params: paramsApi
            });
            
            itemStorages.value = response.data.result
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.result, { position: 'top' })
        }
    }

    async function get(id: string){
        try {
            const response = await axios.get('/api/admin/item-storages/' + id);
            
            itemStorage.value = response.data.result
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.result, { position: 'top' })
        }
    }

    async function insert(param: ItemStorageInfo){
        try {
            const response = await axios.post('/api/admin/item-storages/', { 
                ...param,
                rent_amount: param.rent_service.amount
            });

            $toast.success(response.data.result, { position: 'top' })
            router.push({ name: 'item_storage' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

    async function update(param: ItemStorageInfo){
        try {
            const response = await axios.patch('/api/admin/item-storages/' + param.id, {
                ...param,
                rent_amount: param.rent_service.amount
            });

            $toast.success(response.data.result, { position: 'top' })
            router.push({ name: 'item_storage' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

    async function remove(id: string){
        try {
            const response = await axios.delete('/api/admin/item-storages/' + id);

            $toast.success(response.data.result, { position: 'top' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

    function $reset(){
        itemStorage.value = initialStateItemStorage
    }

    return { itemStorages, itemStorage, getAll, get, insert, update, remove, $reset }
})