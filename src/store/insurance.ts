import { GetParamApi } from "@/interfaces/param"
import router from "@/router";
// import axios, { AxiosError } from "axios"
import axios, { AxiosError } from "../plugins/axiosInstance";
import _ from "lodash";
import { defineStore } from "pinia"
import { ref } from "vue"
import { useToast } from 'vue-toast-notification';

export const useInsuraceStore = defineStore('insurance', () => {

    const $toast = useToast()

    const initialState = {
        id: null,
        tier_name: '',
        description: '',
        coverage_amount: 0
    }

    const insurances = ref<ResponseInsuraceApi>({
        current_page: 0,
        data: [],
        to: 0,
        total: 0
    })
    const insurance = ref<InsuranceInfo>(initialState)

    async function getAll(paramsApi: GetParamApi = {
        page: 1,
        per_page: 5,
        filters: [],
        orders: [],
        keyword: ''
    }) {
        try {
            const response = await axios.get('/api/admin/insurances/', {
                params: paramsApi
            });

            insurances.value = response.data.result
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.result, { position: 'top' })
        }
    }

    async function get(id: string) {
        try {
            const response = await axios.get('/api/admin/insurances/' + id);

            insurance.value = response.data.result
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.result, { position: 'top' })
        }
    }

    async function insert(param: InsuranceInfo) {
        try {
            const response = await axios.post('/api/admin/insurances/', {
                ...param,
            });

            $toast.success(response.data.result, { position: 'top' })
            router.push({ name: 'insurance' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

    async function update(param: InsuranceInfo) {
        try {
            const response = await axios.patch('/api/admin/insurances/' + param.id, {
                ...param,
            });

            $toast.success(response.data.result, { position: 'top' })
            router.push({ name: 'insurance' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

    async function remove(id: string) {
        try {
            const response = await axios.delete('/api/admin/insurances/' + id);

            $toast.success(response.data.result, { position: 'top' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

    function $reset() {
        insurance.value = initialState
    }

    return { insurances, insurance, getAll, get, insert, update, remove, $reset }
})