// import { AxiosError } from "axios";
import axios, { AxiosError } from "../plugins/axiosInstance";
import { defineStore } from "pinia";
import { reactive } from "vue";
import _ from "lodash";

export const useErrorStore = defineStore('error', () => {
    const errors: { items: { [key: string]: string }; } = reactive({
        items: {},
    });

    function set(errorResponse: any | AxiosError){
        _.forEach(errorResponse.response.data.errors, (value: string, key: string) => {
            errors.items[key] = typeof value === 'object' ? value[0] : value;
        });
    }
    
    return { errors, set }
});