import { defineStore } from "pinia";
import { ref } from "vue";

export const useDetailCellStore = defineStore('detail_cell', () => {
    const cellInfo = ref<CellInfo>({
        zone: '00',
        row: 0,
        col: 0
    });

    return { cellInfo };
});
