import { defineStore } from "pinia";
import { ref } from "vue";

export const useDialogStore = defineStore('dialog', () => {
    const show = ref(false);
    const type = ref('delete')

    const resolvePromise = ref()
    const rejectPromise = ref()

    function toggle(){
        show.value = !show.value;
    }

    function prompt(dialogType: string = 'delete'){
        type.value = dialogType
        toggle()
        return new Promise((resolve, reject) => {
            resolvePromise.value = resolve
            rejectPromise.value = reject
        })
    }

    function confirm(){
        resolvePromise.value(true)
        toggle()
    }

    function cancel(){
        resolvePromise.value(false)
        toggle()
    }

    return { show, type, toggle, prompt, confirm, cancel };
});