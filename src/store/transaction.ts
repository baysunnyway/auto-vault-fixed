import { GetParamApi } from "@/interfaces/param"
import router from "@/router";
// import axios, { AxiosError } from "axios"
import axios, { AxiosError } from "../plugins/axiosInstance";
import _ from "lodash";
import { defineStore } from "pinia"
import { ref } from "vue"
import { useToast } from 'vue-toast-notification';

export const useTransactionStore = defineStore('transaction', () => {

    const $toast = useToast()

    const initialState: TransanctionInfo = {
        id: null,
        grand_amount: 0,
        reference_number: "",
        status: "",
        booking_start_at: new Date(),
        created_at: new Date(),
        updated_at: new Date(),
        user: null
    }

    const transactions = ref<ResponseTransactionApi>({
        current_page: 0,
        data: [],
        to: 0,
        total: 0
    })
    const transaction = ref<TransanctionInfo>(initialState)

    async function getAll(paramsApi: GetParamApi = {
        page: 1,
        per_page: 5,
        filters: [],
        orders: [],
        keyword: ''
    }) {
        try {
            const response = await axios.get('/api/admin/transactions/', {
                params: paramsApi
            });

            transactions.value = response.data.result
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.result, { position: 'top' })
        }
    }

    async function get(id: string){
        try {
            const response = await axios.get('/api/admin/transactions/' + id);
            
            transaction.value = response.data.result
            
        } catch (error: any | AxiosError) {
            errorStore.set(error);
        }
    }

    async function payment(id: string, param: { amount: number, payment_method: { title: string, value: string } }) {
        try {
            const response = await axios.post('/api/admin/transactions/' + id + '/payment', {
                amount: param.amount,
                payment_method: param.payment_method.value
            });

            $toast.success(response.data.result, { position: 'top' })
            router.push({ name: 'transaction' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

    async function reject(id: string) {
        try {
            const response = await axios.post('/api/admin/transactions/' + id + '/reject');

            $toast.success(response.data.result, { position: 'top' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

    async function refund(id: string) {
        try {
            const response = await axios.post('/api/admin/transactions/' + id + '/refund');

            $toast.success(response.data.result, { position: 'top' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

    function $reset() {
        transaction.value = initialState
    }

    return { transactions, transaction, getAll, get, payment, reject, refund, $reset }
})