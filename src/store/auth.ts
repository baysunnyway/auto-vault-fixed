import router from "@/router";
// import axios, { AxiosError } from "axios";
import axios, { AxiosError } from "../plugins/axiosInstance";
import { defineStore } from "pinia";
import { ref } from "vue";
import { useErrorStore } from "./error";
import { useUserStore } from "./user";

export const useAuthStore = defineStore('auth', () => {

    const userStore = useUserStore();
    const errorStore = useErrorStore();

    const accessToken = ref(localStorage.getItem('access_token'));

    async function login(username: string, password: string) {
        try {
            const response = await axios.post('/api/auth/token/login', {
                username: username,
                password: password,
                fcm_token: 'FCM_TOKEN'
            });
  
            saveAccessToken(response.data.result.token);
            userStore.set(response.data.result.user);

            axios.defaults.headers['Authorization'] = 'Bearer ' + response.data.result.token;

            router.push({ name: 'dashboard' });
        } catch (error: any | AxiosError) {
            errorStore.set(error);
        }
    }

    function saveAccessToken(responseAccessToken: string) {
        accessToken.value = responseAccessToken;
        localStorage.setItem("access_token", responseAccessToken);
    }

    return { accessToken, login }
});