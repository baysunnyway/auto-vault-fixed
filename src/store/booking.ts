import { GetParamApi } from "@/interfaces/param"
import router from "@/router";
// import axios, { AxiosError } from "axios"
import axios, { AxiosError } from "../plugins/axiosInstance";
import _ from "lodash";
import { defineStore } from "pinia"
import { ref } from "vue"
import { useToast } from 'vue-toast-notification';

export const useBookingStore = defineStore('booking', () => {

    const $toast = useToast()

    const initialStateBooking:BookingInfo  = {
        id: 0,
        slot_type: '',
        status: '',
        insured_value: 0,
        remark: null,
        start_at: new Date(),
        end_at: new Date(),
        transaction: {
            id: 0,
            grand_amount: 0,
            reference_number: '',
            status: '',
            booking_start_at: new Date(),
            created_at: new Date(),
            updated_at: new Date(),
            user: null
        },
        vehicle: null,
        user: null,
        slot: null,
        appointments: null
    }

    const bookings = ref<ResponseBookingApi>({
        current_page: 0,
        data: [],
        to: 0,
        total: 0
    })
    const booking = ref<BookingInfo>(initialStateBooking)

    async function getAll(paramsApi: GetParamApi = {
        page: 1,
        per_page: 5,
        filters: [],
        orders: [],
        keyword: ''
    }) {
        try {
            const response = await axios.get('/api/admin/bookings/', {
                params: paramsApi
            });

            bookings.value = response.data.result
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.result, { position: 'top' })
        }
    }

    async function get(id: string) {
        try {
            const response = await axios.get('/api/admin/bookings/' + id);

            booking.value = response.data.result
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.result, { position: 'top' })
        }
    }

    async function remove(id: string) {
        try {
            const response = await axios.delete('/api/admin/bookings/' + id);

            $toast.success(response.data.result, { position: 'top' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

    function $reset() {
        booking.value = initialStateBooking
    }

    return { bookings, booking, getAll, get, remove, $reset }
})
