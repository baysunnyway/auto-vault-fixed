import { GetParamApi } from "@/interfaces/param";
// import axios, { AxiosError } from "axios";
import axios, { AxiosError } from "../plugins/axiosInstance";
import { defineStore } from "pinia";
import { ref } from "vue";
import { useErrorStore } from "./error";
import {useToast} from 'vue-toast-notification';
import router from "@/router";
import { constant } from "@/config/constant";

export const useStaffStore = defineStore('staff', () => {

    const errorStore = useErrorStore();
    const $toast = useToast()

    const initialStaffState = {
        id: null,
        name: '',
        gender: '',
        nickname: '',
        email: '',
        email_verified_at: new Date(),
        phone_number: '',
        profile_image_url: '',
        is_membership_active: true,
        role: constant.ROLE.STAFF,
        password: '',
        password_confirmation: '',
        company_information: {
            company_name: ''
        },
        customer_information: {
            customer_type: '',
            identity_number: '',
            identity_type: '',
            refferal_name: '',
            refferal_source: '',
            referral_description: ''
        }
    }

    const staffs = ref<ResponseStaffApi>({
        current_page: 0,
        data: [],
        to: 0,
        total: 0
    })
    const staff = ref<StaffInfo>(initialStaffState)

    async function getAll(paramsApi: GetParamApi = { 
            page: 1,
            per_page: 5,
            filters: [],
            orders: [],
            keyword: ''
        }) {
        try {
            const response = await axios.get('/api/admin/users', {
                params: paramsApi
            });
            
            staffs.value = response.data.result
            staffs.value.data.forEach(staff => {
              if (staff.customer_information === null) {
                staff.customer_information = {
                  customer_type: '',
                  identity_number: '',
                  identity_type: '',
                  refferal_name: '',
                  refferal_source: ''
                };
              }
            });
        } catch (error: any | AxiosError) {
            errorStore.set(error);
        }
    }

    async function get(id: string){
        try {
            const response = await axios.get('/api/admin/users/' + id);
            
            staff.value = response.data.result
            if(staff.value.customer_information === null){
                staff.value.customer_information = initialStaffState.customer_information
            }
            if(staff.value.company_information === null){
                staff.value.company_information = initialStaffState.company_information
            }
        } catch (error: any | AxiosError) {
            errorStore.set(error);
        }
    }

    async function insert(staffParam: StaffInfo){
        try {
            const response = await axios.post('/api/admin/users/', {
                ...staffParam,
                role_id: staffParam.role.id
            });

            $toast.success(response.data.result, { position: 'top' })
            router.push({ name: 'staff' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

    async function update(staffParam: StaffInfo){
        try {
            const response = await axios.patch('/api/admin/users/' + staffParam.id, {
                ...staffParam,
                role_id: staffParam.role.id,
                customer_type: staffParam.customer_information.customer_type,
                identity_number: staffParam.customer_information.identity_number,
                identity_type: staffParam.customer_information.identity_type,
                referral_name: staffParam.customer_information.referral_name,
                referral_source: staffParam.customer_information.referral_source,
            });
            console.log('staf')
            console.log(staffParam)
            $toast.success(response.data.result, { position: 'top' })
            router.push({ name: 'staff' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

    async function remove(staffParam: StaffInfo){
        try {
            const response = await axios.patch('/api/admin/users/' + staffParam.id, {
                ...staffParam,
                role_id: staffParam.role.id
            });
            console.log('staf')
            console.log(staffParam)
            $toast.success(response.data.result, { position: 'top' })
            router.push({ name: 'staff' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

    function $reset(){
        staff.value = initialStaffState
    }

    return { staffs, staff, getAll, get, insert, update, $reset };
})
