import { GetParamApi } from "@/interfaces/param"
import router from "@/router";
// import axios, { AxiosError } from "axios"
import axios, { AxiosError } from "../plugins/axiosInstance";
import _ from "lodash";
import { defineStore } from "pinia"
import { ref } from "vue"
import { useToast } from 'vue-toast-notification';

export const useVehicleStore = defineStore('vehicle', () => {

    const $toast = useToast()

    const initialState = {
        id: null,
        nickname: '',
        brand: '',
        model: '',
        year: '',
        color: '',
        license_plate_number: '',
        value: 0,
        transaction_insurance_flag: false,
        engine_warm_up_care: 0,
        engine_warm_up_care_frequency: 0,
        tire_pressure_check_care: 0,
        rear_tire_pressure: 0,
        front_tire_pressure: 0,
        accu_battery_charging_care: 0,
        accu_battery_charging_note: '',
        deleted_at: null,
        created_at: new Date(),
        updated_at: new Date(),
        special_requests: [],
        attachments: [],
        tags: [],
        cares: [],
        user: null
    }

    const vehicles = ref<ResponseVehicleApi>({
        current_page: 0,
        data: [],
        to: 0,
        total: 0
    })
    const vehicle = ref<VehicleInfo>(initialState)

    async function getAll(paramsApi: GetParamApi = {
        page: 1,
        per_page: 5,
        filters: [],
        orders: [],
        keyword: ''
    }) {
        try {
            const response = await axios.get('/api/admin/vehicles/', {
                params: paramsApi
            });

            vehicles.value = response.data.result
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.result, { position: 'top' })
        }
    }

    async function get(id: string) {
        try {
            const response = await axios.get('/api/admin/vehicles/' + id);

            vehicle.value = response.data.result
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.result, { position: 'top' })
        }
    }

    function $reset() {
        vehicle.value = initialState
    }

    return { vehicles, vehicle, getAll, get, $reset }
})