import { GetParamApi } from "@/interfaces/param";
// import axios, { AxiosError } from "axios";
import axios, { AxiosError } from "../plugins/axiosInstance";
import { defineStore } from "pinia";
import { ref } from "vue";
import { useErrorStore } from "./error";
import {useToast} from 'vue-toast-notification';
import { ResponseStorageApi, StorageInfo } from '@/store/types/storage'

export const useStorageStore = defineStore('storage', () => {
		
		const errorStore = useErrorStore();
		const $toast = useToast()
		
		const initialStorageState: StorageInfo = {
				id: null,
				code: '',
				name: '',
				status: '',
				length: 0,
				width: 0,
				height: 0,
				total: 0,
				data: undefined
		}
		
		const storages = ref<ResponseStorageApi>({
				current_page: 0,
				data: [],
				to: 0,
				total: 0
		})
		const storage = ref<StorageInfo>(initialStorageState)
		
		async function getAll(paramsApi: GetParamApi = {
				filters: undefined,
				page: 1,
				per_page: 5,
				orders: [],
				keyword: ''
		}) {
				try {
						const response = await axios.get('/api/admin/item-storages', {
								params: paramsApi
						});
						
						storage.value = response.data.result
				} catch (error: any | AxiosError) {
						errorStore.set(error);
				}
		}
		
		async function get(id: string){
				try {
						const response = await axios.get('/api/admin/item-storages/' + id);
						
						storage.value = response.data.result
				} catch (error: any | AxiosError) {
						errorStore.set(error);
				}
		}
		
		async function update(storageInfo: StorageInfo){}
		
		async function insert(storageInfo: StorageInfo) {}
		
		
		function $reset(){
				storage.value = initialStorageState
		}
		
		return { storages: storages, storage: storage, getAll, get, update, insert,  $reset };
})
