import { GetParamApi } from "@/interfaces/param"
import router from "@/router";
// import axios, { AxiosError } from "axios"
import axios, { AxiosError } from "../plugins/axiosInstance";
import _ from "lodash";
import { defineStore } from "pinia"
import { ref } from "vue"
import { useToast } from 'vue-toast-notification';

export const useAppointmentStore = defineStore('appointment', () => {

    const $toast = useToast()

    const initialState: AppointmentInfo = {
        id: null,
        reference_number: '',
        type: '',
        date_time: new Date(),
        status: '',
        is_delegated: false,
        completed_at: null,
        created_at: new Date(),
        updated_at: new Date(),
        booking: null,
        driver: null,
        staff: null,
        vehicle: null
    }

    const appointments = ref<ResponseAppointmentApi>({
        current_page: 0,
        data: [],
        to: 0,
        total: 0
    })
    const appointment = ref<AppointmentInfo>(initialState)

    async function getAll(paramsApi: GetParamApi = {
        page: 1,
        per_page: 5,
        filters: [],
        orders: [],
        keyword: ''
    }) {
        try {
            const response = await axios.get('/api/admin/appointments/', {
                params: paramsApi
            });

            appointments.value = response.data.result
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.result, { position: 'top' })
        }
    }

    async function get(reference_number: string) {
        try {
            const response = await axios.get('/api/admin/appointments/' + reference_number);

            appointment.value = response.data.result
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.result, { position: 'top' })
        }
    }

    async function update(reference_number: string, status: string) {
        try {
            const response = await axios.patch('/api/admin/appointments/' + reference_number, {
                status: status,
            });

            $toast.success(response.data.result, { position: 'top' })
            router.push({ name: 'appointment' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

    function $reset() {
        appointment.value = initialState
    }

    return { appointments, appointment, getAll, get, update, $reset }
})