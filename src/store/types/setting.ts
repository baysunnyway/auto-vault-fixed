interface ResponseSettingApi {
    current_page: number
    data: Array<SettingInfo>
    to: number
    total: number
}

interface SettingInfo {
    id: number | null
    display_name: string
    value: string
}