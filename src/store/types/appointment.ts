interface ResponseAppointmentApi {
    current_page: number
    data: Array<AppointmentInfo>
    to: number
    total: number
}

interface AppointmentInfo {
    id: number | null
    reference_number: string
    type: string
    date_time: Date
    status: string
    is_delegated: boolean
    completed_at: Date | null
    created_at: Date
    updated_at: Date
    booking: BookingInfo | null
    driver: DriverInfo | null
    staff: StaffInfo | null
    vehicle: VehicleInfo | null
}