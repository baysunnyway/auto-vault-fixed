interface SpecialRequestInfo {
    id: number | null
    title: string
    description: string | null
}