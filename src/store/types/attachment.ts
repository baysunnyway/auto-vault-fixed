interface AttachmentInfo {
    id: number | null
    type: string
    created_at: Date
    updated_at: Date
    url: string
}