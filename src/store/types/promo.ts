type PromoInfo = {
		id: null,
		is_active: boolean,
		title: string,
		description: string,
		promo_type: PromoTypeInfo,
		amount: number,
		code: string,
		is_combinable: boolean,
		quota: number,
		started_at: string,
		ended_at: string,
		created_at: string,
		usages_count: number
}

type PromoTypeInfo = {
	id: string
	name: string
}
