type StaffInfo = {
		id: number | null
		name: string
		gender: string
		nickname: string
		email: string
		email_verified_at: Date
		phone_number: string
		profile_image_url: string
		is_membership_active: boolean
		password: string
		password_confirmation: string
		role: {
				id: number
				name: string
		}
		customer_information: {
				customer_type: string
				refferal_name: string
				refferal_source: string
		}
}

type ResponseStaffApi = {
		current_page: number
		data: Array<StaffInfo>
		to: number
		total: number
}
