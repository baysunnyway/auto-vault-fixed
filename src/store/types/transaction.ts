interface ResponseTransactionApi {
    current_page: number
    data: Array<TransanctionInfo>
    to: number
    total: number
}

interface TransanctionInfo {
    id: number | null
    grand_amount: number
    reference_number: string
    status: string
    booking_start_at: Date
    created_at: Date
    updated_at: Date
    user: null
}