interface ResponseItemStorageApi {
    current_page: number
    data: Array<ItemStorageInfo>
    to: number
    total: number
}

interface ItemStorageInfo {
    id: number | null
    code: string
    name: string
    length: number
    width: number
    height: number
    status: string,
    rent_service: {
        id: number
        rentable_type: string
        amount: number
    }
}