interface ResponseVehicleApi {
    current_page: number
    data: Array<VehicleInfo>
    to: number
    total: number
}

interface VehicleInfo {
    id: number | null
    nickname: string
    brand: string
    model: string
    year: string
    color: string
    license_plate_number: string
    value: number
    transaction_insurance_flag: boolean
    engine_warm_up_care: number
    engine_warm_up_care_frequency: number
    tire_pressure_check_care: number
    rear_tire_pressure: number
    front_tire_pressure: number
    accu_battery_charging_care: number
    accu_battery_charging_note: string
    deleted_at: Date | null
    created_at: Date
    updated_at: Date
    special_requests: Array<SpecialRequestInfo>
    attachments: Array<AttachmentInfo>
    tags: Array<TagInfo> 
    cares: Array<CareInfo>
    user: UserInfo | null
}