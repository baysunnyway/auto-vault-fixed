interface ResponseInsuraceApi {
    current_page: number
    data: Array<InsuranceInfo>
    to: number
    total: number
}

interface InsuranceInfo {
    id: number | null
    tier_name: string
    description: string
    coverage_amount: number
}