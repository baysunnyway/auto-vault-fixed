type RoleInfo = {
	id: Number,
	name: String
}

type UserInfo = {
	id: number
	name: string
	email: string
	is_membership_active: boolean
	profile_image_url: string
	role: Array<RoleInfo>
}
