interface ResponseVehicleCareApi {
    current_page: number
    data: Array<VehicleCareInfo>
    to: number
    total: number
}

interface VehicleCareInfo {
    id: number | null
    code_name: string | null
    title: string
    description: string
    status: string
    due_date: Date
    fulfilled_at: Date
    created_at: Date
    vehicle: VehicleInfo | null
}