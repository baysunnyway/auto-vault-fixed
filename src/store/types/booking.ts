interface ResponseBookingApi {
    current_page: number
    data: Array<BookingInfo>
    to: number
    total: number
}

interface BookingInfo {
    id: number | null
    slot_type: string
    status: string
    insured_value: number
    remark: string | null,
    start_at: Date
    end_at: Date
    transaction: TransanctionInfo
    vehicle: VehicleInfo | null
    user: UserInfo | null
    slot: SlotInfo | null
    appointments: Array<AppointmentInfo> | null
}