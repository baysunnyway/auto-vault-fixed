interface CareInfo {
    id: number | null
    title: string
    description: string
    created_at: Date
    updated_at: Date
    attachments: Array<AttachmentInfo>
    staff: StaffInfo | null
}