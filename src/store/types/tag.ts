interface TagInfo {
    id: number | null
    name: string
    description: string
}