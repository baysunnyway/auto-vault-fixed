interface DriverInfo {
    id: number | null
    name: string
    nickname: string
    gender: string
    identity_card_number: string
    profile_image_url: string
}