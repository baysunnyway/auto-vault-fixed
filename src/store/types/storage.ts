export type StorageInfo = {
		id: null,
		code: string,
		name: string,
		status: string,
		length: number,
		width: number,
		height: number
		data: any
		total: number
}
export enum status {
		VACANT = 'VACANT',
		OCCUPIED = 'occupied'
}
export type ResponseStorageApi = {
		current_page: number
		data: Array<StorageInfo>
		to: number
		total: number
}
