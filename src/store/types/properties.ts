type serverOptions = {
		page: number,
		rowsPerPage: number,
		sortBy: string,
		sortType: string
}
