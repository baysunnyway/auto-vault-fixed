interface SlotInfo {
    id: number
    code: string
    directory_level: string
    directory_row: string
    directory_column: string
    type: string
    status: string
}