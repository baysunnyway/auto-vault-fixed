import { GetParamApi } from "@/interfaces/param"
import router from "@/router";
// import axios, { AxiosError } from "axios"
import axios, { AxiosError } from "../plugins/axiosInstance";
import _ from "lodash";
import { defineStore } from "pinia"
import { ref } from "vue"
import { useToast } from 'vue-toast-notification';

export const useVehicleCareStore = defineStore('vehicle_care', () => {

    const $toast = useToast()

    const initialState = {
        id: null,
        code_name: null,
        title: '',
        description: '',
        status: '',
        due_date: new Date(),
        fulfilled_at: new Date(),
        created_at: new Date(),
        vehicle: null
    }

    const vehicleCares = ref<ResponseVehicleCareApi>({
        current_page: 0,
        data: [],
        to: 0,
        total: 0
    })
    const vehicleCare = ref<VehicleCareInfo>(initialState)

    async function getAll(paramsApi: GetParamApi = {
        page: 1,
        per_page: 5,
        filters: [],
        orders: [],
        keyword: ''
    }) {
        try {
            const response = await axios.get('/api/admin/tasks/', {
                params: paramsApi
            });

            vehicleCares.value = response.data.result
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.result, { position: 'top' })
        }
    }

    async function get(id: string) {
        try {
            const response = await axios.get('/api/admin/tasks/' + id);

            vehicleCare.value = response.data.result
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.result, { position: 'top' })
        }
    }

    async function insert(param: VehicleCareInfo) {
        try {
            const response = await axios.post('/api/admin/tasks/', param);

            $toast.success(response.data.result, { position: 'top' })
            router.push({ name: 'task' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

    async function update(param: VehicleCareInfo) {
        try {
            const response = await axios.patch('/api/admin/tasks/' + param.id, param);

            $toast.success(response.data.result, { position: 'top' })
            router.push({ name: 'task' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

    async function complete(id: string) {
        try {
            const response = await axios.post('/api/admin/tasks/' + id + '/complete');

            $toast.success(response.data.result, { position: 'top' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

    async function remove(id: string) {
        try {
            const response = await axios.delete('/api/admin/tasks/' + id);

            $toast.success(response.data.result, { position: 'top' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

    function $reset() {
        vehicleCare.value = initialState
    }

    return { vehicleCares, vehicleCare, getAll, get, insert, update, complete, remove, $reset }
})