import { GetParamApi } from "@/interfaces/param";
// import axios, { AxiosError } from "axios";
import axios, { AxiosError } from "../plugins/axiosInstance";
import { defineStore } from "pinia";
import { ref } from "vue";
import { useErrorStore } from "./error";
import {useToast} from 'vue-toast-notification';
import router from "@/router";
import { constant } from "@/config/constant";

export const useCustomerStore = defineStore('customer', () => {

    const errorStore = useErrorStore();
    const $toast = useToast()

    const initialCustomerState = {
        id: null,
        name: '',
        gender: '',
        nickname: '',
        email: '',
        email_verified_at: new Date(),
        phone_number: '',
        points: 0,
        profile_image_url: '',
        is_membership_active: true,
        role: constant.ROLE.CUSTOMER,
        password: '',
        password_confirmation: '',

        company_information: {
            company_name: ''
        },

        customer_information: {
            customer_type: '',
            identity_number: '',
            identity_type: '',
            referral_name: '',
            referral_source: '',
            referral_description: ''
        }
    }

    const customers = ref<ResponseCustomerApi>({
        current_page: 0,
        data: [],
        to: 0,
        total: 0
    })
    const customer = ref<CustomerInfo>(initialCustomerState)

    async function getAll(paramsApi: GetParamApi = {
        page: 1,
        per_page: 5,
        filters: [],
        orders: [],
        keyword: ''
    }) {
        try {
            const response = await axios.get('/api/admin/users', {
                params: paramsApi
            });
            
            customers.value = response.data.result
        } catch (error: any | AxiosError) {
            errorStore.set(error);
        }
    }

    async function get(id: string){
        try {
            const response = await axios.get('/api/admin/users/' + id);
            
            customer.value = response.data.result
            if(customer.value.customer_information === null){
                customer.value.customer_information = initialCustomerState.customer_information
            }
            if(customer.value.company_information === null){
                customer.value.company_information = initialCustomerState.company_information
            }
        } catch (error: any | AxiosError) {
            errorStore.set(error);
        }
    }

    async function insert(customerParam: CustomerInfo){
        try {
            const response = await axios.post('/api/admin/users/', {
                ...customerParam,
                role_id: customerParam.role.id
            });
            $toast.success(response.data.result, { position: 'top' })
            router.push({ name: 'customer' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

    async function update(customerParam: CustomerInfo){
        try {
            const response = await axios.patch('/api/admin/users/' + customerParam.id, {
                ...customerParam,
                company_name: customerParam.customer_information.customer_type === 'business' ? customerParam.company_information.company_name : '-',
                customer_type: customerParam.customer_information.customer_type,
                identity_number: customerParam.customer_information.identity_number,
                identity_type: customerParam.customer_information.identity_type,
                referral_name: customerParam.customer_information.referral_name,
                referral_source: customerParam.customer_information.referral_source,
                referral_description: customerParam.customer_information.referral_description,
                role_id: customerParam.role.id
            });
            
            $toast.success(response.data.result, { position: 'top' })
            router.push({ name: 'customer' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

    async function addPoint(customerParam: CustomerInfo){
        try {
            const response = await axios.post('/api/admin/users/' + customerParam.id + '/points', {
                
                amount: customerParam.points,
                
            });
            
            $toast.success(response.data.result, { position: 'top' })
            router.push({ name: 'customer' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.message, { position: 'top' })
        }
    }

    async function updateMembership(customerParam: CustomerInfo){
        try {
            const response = !customerParam.is_membership_active ? 
                await axios.post('/api/admin/users/' + customerParam.id + '/memberships/block') :
                await axios.post('/api/admin/users/' + customerParam.id + '/memberships/approval', {is_approved: 1});
            
            $toast.success(response.data.result, { position: 'top' })
            router.push({ name: 'customer' })
        } catch (error: any | AxiosError) {
            $toast.error(error.response.data.result, { position: 'top' })
        }
    }
    function $reset(){
        customer.value = initialCustomerState
    }

    return { customers, customer, getAll, get, insert, update, addPoint, updateMembership, $reset };
})
